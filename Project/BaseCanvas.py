from tkinter import Tk, Button
class BaseCanvas:
    def __init__(self,window):
        self.create_canvas(window)
        self.add_features()

    def create_canvas(self,window):
        self.window = window
        self.window.wm_protocol("WM_DELETE_WINDOW", lambda: self.window.destroy())
        self.window.title('Data Quality Tool')  # TITLE
        self.window.geometry("730x400")  # WINDOW SIZE
        self.window.resizable(False, False)  # SETS STATUS OF WINDOW'S RESIZEABLE FEATURE
    def add_features(self):
        #WIDGETS
        loadFileButton = Button(self.window, text="A BUTTON", command=lambda: print('Button Pressed'),
                                width=105, height=1,
                                bg="black", fg="white", )
        #PACKING WIDGETS IN WINDOW
        loadFileButton.pack()
        #PLACING WIDGETS IN CERTAIN CORDS.
        loadFileButton.place(x=0, y=0)
# END OF FUNCTIONS FOR WIDGETS ON UI-------------------------------------------------------------------------------------------------------------------------------------
#EXECUTE
root = Tk()
gui = BaseCanvas(root)
root.mainloop()








